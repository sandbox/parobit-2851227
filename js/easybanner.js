/**
 * @file
 * Main module js file.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.easyBanner = {
    attach: function (context, settings) {
      if (typeof (Drupal.settings.easyBanner) !== 'undefined') {
        // Set up  required elements.

        this.container = $('<div id="simple-banner" />').appendTo('body');
        this.wrapper = $('<div id="simple-banner-wrapper" />')
                        .appendTo(this.container).addClass('simple-banner-wrapper');
        this.linkMarkup = $('<a />', {href: Drupal.settings.easyBanner.link}).appendTo(this.wrapper);
        this.bannerMarkup = $('<img />', {src: Drupal.settings.easyBanner.image}).appendTo(this.linkMarkup);
        this.closer = $('<span class="close">[x]</span>').appendTo(this.wrapper);


        this.opener = $('<div id="simple-banner-opener" />').appendTo('body');
        this.opener.html(Drupal.settings.easyBanner.title).addClass('simple-banner opener');

        if (Drupal.settings.easyBanner.position === 'center') {
          // Create popup.
          this.container.addClass('simple-banner popup-container');

        }
        else {
          // Create banner.
          this.container.addClass('simple-banner banner-container');
        }

        this.setBannerPosition(Drupal.settings.easyBanner.position);

        // Attach event handler
        this.container.bind('click', {easyBanner: this}, this.bannerClickHander);
        this.opener.bind('click', {easyBanner: this}, this.openerClickHander);
        // ----------------------------------------------------------------------
        if ($.cookie('simple_banner_is_closed') === null) {
          this.open();
        }
        else {
          this.opener.fadeIn();
        }

      }

    },
    setBannerPosition: function (position) {
      switch (position) {
        case 'top_left':
          this.container.css({top: 0, left: 0});
          this.opener.css({top: 0, left: 0});
          this.closer.css({bottom: '2px', right: '2px'});
          break;

        case 'top_right':
          this.container.css({top: 0, right: 0});
          this.opener.css({top: 0, right: 0});
          this.closer.css({bottom: '2px', left: '2px'});
          break;

        case 'bottom_left':
          this.container.css({bottom: 0, left: 0});
          this.opener.css({bottom: 0, left: 0});
          this.closer.css({top: '2px', right: '2px'});
          break;

        case 'bottom_right':
          this.container.css({bottom: 0, right: 0});
          this.opener.css({bottom: 0, right: 0});
          this.closer.css({top: '2px', left: '2px'});
          break;
        case 'center':
          this.opener.css({bottom: 0, left: 0});
          this.closer.css({top: '2px', right: '2px'});
          break;

      }
    },
    open: function () {
      this.container.fadeIn();
      this.opener.fadeOut();
    },
    close: function () {
      this.container.fadeOut();
      this.opener.fadeIn();
      this.markAsClosed();
    },
    bannerClickHander: function (e) {
      if (e.target.tagName.toLowerCase() !== 'a') {
        e.data.easyBanner.close();
      }
      else {
        // Banner clicked, we close banner for a shorttime.
        e.data.easyBanner.markAsClosed();
      }
    },
    openerClickHander: function (e) {
      e.data.easyBanner.open();
    },
    markAsClosed: function () {
      var expireTime = new Date();
      expireTime.setMinutes(expireTime.getMinutes() + parseInt(Drupal.settings.easyBanner.closeTime));
      $.cookie('simple_banner_is_closed', 1, {expires: expireTime, path: '/'});
    }
  };

})(jQuery);
